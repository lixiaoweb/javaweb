package com.javaweb.javaweb.model.utils;

/**
 * Description:封装类，与前台页面交互的过程中用来传递数据
 * User: lixiao
 * Date: 2017/10/28
 * Time: 15:57
 * To change this template use File | Settings | File Templates.
 */
public class ResultVo <T>{
   private boolean success = false;
   private String message = null;
   private T result = null;

    public void isSuccess(boolean b) {
        this.success=b;
    }
    public void setMessage(String message) {
        this.message = message;
    }
    public void setResult(T result) {
        this.result = result;
    }
    public boolean getSuccess(){
        return success;
    }
    public String getMessage() {
        return message;
    }
    public T getResult() {
        return result;
    }

}
