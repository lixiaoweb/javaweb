package com.javaweb.javaweb.controller;

import com.javaweb.javaweb.model.Student;
import com.javaweb.javaweb.model.utils.ResultVo;
import com.javaweb.javaweb.service.StudentService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;

/**
 * Description:学生
 * Author: lixiao
 * Date: 2017/10/28
 * Time: 16:13
 * To change this template use File | Settings | File Templates.
 */
@Controller
@RequestMapping("/studentApi/")
public class StudentController {
    @Resource
    private StudentService studentService;
    @RequestMapping("findAllStudentInfo")
    @ResponseBody //添加ResponseBody直接返回json数据
    public ResultVo findAllStudentInfo(@RequestParam("id") String id) {
        Student student = new Student();
        ResultVo resultVo = new ResultVo();
        if ("0".equals(id)) {
            resultVo.isSuccess(false);
            resultVo.setMessage("错误的参数");
        } else {
            student.setId(new Integer(id));
            resultVo = studentService.findAllStudent(student);
        }
        return resultVo;
    }
}
