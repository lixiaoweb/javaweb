package com.javaweb.javaweb.service.impl;

import com.javaweb.javaweb.dao.StudentDao;
import com.javaweb.javaweb.model.Student;
import com.javaweb.javaweb.model.utils.ResultVo;
import com.javaweb.javaweb.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Description:学生实现类
 * User: Administrator
 * Date: 2017/10/28
 * Time: 11:32
 * To change this template use File | Settings | File Templates.
 */
@Service("StudentService")
public class StudentServiceImpl implements StudentService {
    @Resource
    private StudentDao studentDao;
    @Override
    public Boolean addStudent(Student student){
       return studentDao.insert(student);
    }
    @Override
    public Boolean deleteStudentById(Integer id){
        Student student = new Student();
        student.setId(id);
        return studentDao.delete(student);
    }
    @Override
    public Boolean updateStudentById(Student student){
        return studentDao.update(student);
    }
    @Override
    public ResultVo findAllStudent(Student student){
        ResultVo resultVo = new ResultVo();
        List<Student> list = studentDao.select(student);
        if(list.size()>0){
            resultVo.setResult(list);
            resultVo.isSuccess(true);
        }else{
            resultVo.isSuccess(false);
            resultVo.setMessage("没有找到相关信息");
        }
        return resultVo;
    }
}
