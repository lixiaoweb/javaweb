package com.javaweb.javaweb.service;

import com.javaweb.javaweb.model.Student;
import com.javaweb.javaweb.model.utils.ResultVo;

import java.util.List;

/**
 * Created by Administrator on 2017/10/28.
 */
public interface StudentService {
   /**
    * @Description:添加学生
    * @param:student
    * @return:
    * @Author:LiXiao
    * @Date: 11:22 2017/10/28
    */
    Boolean addStudent(Student student);

    /**
     * @Description: 根据id删除学生
     * @param:id
     * @return:
     * @Author:LiXiao
     * @Date: 11:22 2017/10/28
     */
    Boolean deleteStudentById(Integer id);

    /**
     * @Description:根据id修改学生
     * @param:student
     * @return:
     * @Author:LiXiao
     * @Date: 11:27 2017/10/28
     */
    Boolean updateStudentById(Student student);
    
    /**
     * @Description:按条件查找所有学生
     * @param:student
     * @return:
     * @Author:LiXiao
     * @Date: 11:30 2017/10/28
     */
    ResultVo findAllStudent(Student student);
}
