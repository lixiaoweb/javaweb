package com.javaweb.javaweb.dao;

import com.javaweb.javaweb.dao.base.BaseDao;
import com.javaweb.javaweb.model.Student;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Administrator on 2017/10/28.
 */
@Repository
public interface StudentDao extends BaseDao<Student> {

}
