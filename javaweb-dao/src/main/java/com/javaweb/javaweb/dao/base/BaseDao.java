package com.javaweb.javaweb.dao.base;
import java.util.List;

/**
 * Created by Administrator on 2017/10/28.
 */
public interface BaseDao<T> {
   public Boolean insert(T var);
   public Boolean delete(T var);
   public Boolean update(T var);
   public List<T> select(T var);
}
